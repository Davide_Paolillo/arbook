﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    Vector3 originalPos;
    // Start is called before the first frame update
    void Start()
    {
        originalPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // il muro e' un oggetto triggerabile, quindi quando c' e' una collisione viene controllata questa condizione
    private void OnTriggerEnter(Collider collide)
    {
        if (collide.gameObject.CompareTag("SHIPWALL"))
        {
            transform.localPosition = originalPos;
        } else if (collide.gameObject.CompareTag("AIRPLANEWALL"))
        {
            transform.localPosition = originalPos;
        } else if (collide.gameObject.CompareTag("CLOUDWALL"))
        {
            transform.localPosition = originalPos;
        }
    }
}
